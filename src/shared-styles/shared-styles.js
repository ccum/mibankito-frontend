import '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';

const $_documentContainer = document.createElement('template');
$_documentContainer.innerHTML = `<dom-module id="shared-styles">
  <template>
    <style is="custom-style">
      body {
        font-family: 'Roboto', 'Noto', sans-serif;
        font-size: 14px;
        margin: 0;
        background-color: #FAFAFA;
      }
      
      .card {
        background: #FFFFFF;
        padding: 24px;
        margin: 20px;
      }
      .flex-horizontal {
        @apply --layout-horizontal;
      }
      .flexchild {
        @apply --layout-flex;
      }
      .title-section h3{
        text-align: center;
        font-style: italic;
      }
      .income paper-icon-button{
        color: green;
      }
      .outcome paper-icon-button{
        color: red;
      }
      #errorlabel {
        --paper-toast-background-color: red;
        --paper-toast-color: white;
      }
    </style>
  </template>
</dom-module>`;

document.head.appendChild($_documentContainer.content);
