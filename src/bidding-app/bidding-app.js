import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class BiddingApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        cafe-header { @apply --paper-font-headline; }
        .cafe-light { color: var(--paper-grey-600); }
        .cafe-location {
          float: right;
          font-size: 15px;
          vertical-align: middle;
        }
        .cafe-reserve { color: var(--google-blue-500); }
        iron-icon.star {
          --iron-icon-width: 16px;
          --iron-icon-height: 16px;
          color: var(--paper-amber-500);
        }
        iron-icon.star:last-of-type { color: var(--paper-grey-500); }
        paper-card{
          margin: 10px;
          min-width: 380px;
        }
      </style>
      <paper-material class="card" elevation="1">
        <div class="title-section">
          <h3>Estimado cliente este es el conjunto de promociones que tenemos para ti</h3>
        <div>
        <paper-card image="https://www.bbvacontinental.pe/fbin/mult/flora-fauna_tcm1105-769562.png" alt="Donuts" width="48" height="48">
          <div class="card-content">
            <div class="cafe-header">Flora y Fauna
            </div>
            <div class="cafe-rating">
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
            </div>
            <p class="cafe-light">25% de descuento.</p>
          </div>
          <div class="card-actions">
            <a href="http://florayfauna.pe/" tabindex="-1" target="_blank">
              <paper-button class="cafe-reserve">Ver mas</paper-button>
            </a>
          </div>
        </paper-card>
        
        <paper-card image="https://www.bbvacontinental.pe/fbin/mult/lana-bang_tcm1105-634044.jpg" alt="Donuts" width="48" height="48">
          <div class="card-content">
            <div class="cafe-header">Lana Bang Store
            </div>
            <div class="cafe-rating">
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
            </div>
            <p class="cafe-light">30% de descuento.</p>
          </div>
          <div class="card-actions">
            <a href="https://es-la.facebook.com/LanaBang/" tabindex="-1" target="_blank">
              <paper-button class="cafe-reserve">Ver mas</paper-button>
            </a>
          </div>
        </paper-card>

        <paper-card image="https://www.bbvacontinental.pe/fbin/mult/adidas-originals_tcm1105-764561.png" alt="Donuts" width="48" height="48">
          <div class="card-content">
            <div class="cafe-header">Adidas
            </div>
            <div class="cafe-rating">
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
            </div>
            <p class="cafe-light">25% de descuento.</p>
          </div>
          <div class="card-actions">
            <a href="http://florayfauna.pe/" tabindex="-1" target="_blank">
              <paper-button class="cafe-reserve">Ver mas</paper-button>
            </a>
          </div>
        </paper-card>

        <paper-card image="https://www.bbvacontinental.pe/fbin/mult/baby-modas_tcm1105-672312.jpg" alt="Donuts" width="48" height="48">
          <div class="card-content">
            <div class="cafe-header">Baby Modas
            </div>
            <div class="cafe-rating">
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
            </div>
            <p class="cafe-light">15% de descuento.</p>
          </div>
          <div class="card-actions">
            <a href="http://florayfauna.pe/" tabindex="-1" target="_blank">
              <paper-button class="cafe-reserve">Ver mas</paper-button>
            </a>
          </div>
        </paper-card>

        <paper-card image="https://www.bbvacontinental.pe/fbin/mult/ac-tours_tcm1105-769312.png" alt="Donuts" width="48" height="48">
          <div class="card-content">
            <div class="cafe-header">AC Tours
            </div>
            <div class="cafe-rating">
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
              <iron-icon class="star" icon="star"></iron-icon>
            </div>
            <p class="cafe-light">10% de descuento.</p>
          </div>
          <div class="card-actions">
            <a href="http://florayfauna.pe/" tabindex="-1" target="_blank">
              <paper-button class="cafe-reserve">Ver mas</paper-button>
            </a>
          </div>
        </paper-card>
      </paper-material>
    `;
  }
  static get properties() {
    return {
    };
  }


  
}

window.customElements.define('bidding-app', BiddingApp);
