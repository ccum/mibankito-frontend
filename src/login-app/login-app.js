import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-material/paper-material.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-input/iron-input.js';
import '@polymer/iron-localstorage/iron-localstorage.js';

import '@polymer/paper-button/paper-button.js'
import '@polymer/paper-input/paper-input.js'
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-toast/paper-toast.js';
import '@polymer/gold-cc-input/gold-cc-input.js';
import '../shared-styles/shared-styles.js';
/**
 * @customElement
 * @polymer
 */
class LoginApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      <style>
      :host {
        display: block;
      }
      .form-title {
          margin-bottom: 20px;
      }
      
      .avatar {
        display: inline-block;
        width: 60px;
        height: 60px;
        border-radius: 50%;
        overflow: hidden;
        background: var(--app-secondary-color);
        margin: 0 45%;
      }
      .avatar img {
        margin: 10px;
      }
        .company {
          font-size: 20px;
          font-weight: 200;
          text-align: center;
        }
        paper-button {
          background-color: var(--btn-primary-color);
          color: #fff;
          margin-top: 50px;
          width: 46%;
        }
        .login-form {
          max-width: 600px;
          margin: 0 auto;
          padding: 20px;
          background: #FFFFFF;
        }
        .cards-account a:visited, a, a:active {
          color: black;
        }
      </style>
      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      <iron-ajax
        id="loginAjax"
        method="post"
        content-type="application/json"
        handle-as="text"
        on-response="handleUserResponse"
        on-error="handleUserError">
      </iron-ajax>
      <paper-material class="login-form" elevation="1">
        <div class="avatar" item-icon>
          <img class="mr-3" src="https://getbootstrap.com/docs/4.2/assets/brand/bootstrap-outline.svg" alt="" width="40" height="40">
        </div>
        <div class="flex company">Bienvenido</div>
        <form is="iron-form" id="form" method="" action="">
          <paper-input label="Número de Documento:" value="{{formData.documentNumber}}"></paper-input>
          <paper-input label="Contraseña:" type="password" value="{{formData.password}}"></paper-input>
          <paper-button raised class="primary" on-tap="postLogin">Ingresar</paper-button>
          <a href="/user" tabindex="-1">
            <paper-button class="link" on-tap="postRegister">Registrar</paper-button>
          </a>
          
        </form>
        <paper-toast id="errorlabel" class="fit-bottom" text="This toast is red and fits bottom!"></paper-toast>
      </paper-material>
    `;

  }
  static get properties() {
    return {
      formData: {
        type: Object,
        value: {}
      },
      storedUser: {
        type: Object,
        notify: true
      },
      urlServiceBase:{
        type: String,
        value: "https://mv5lte7w8c.execute-api.us-west-2.amazonaws.com/dev/"
      }
    };
  }
  _setReqBody(){
    console.log(this.formData);
    this.$.loginAjax.body = this.formData;

  }
  postLogin(){
    console.log("holaaa");
    this.$.loginAjax.url = this.urlServiceBase + 'login';
    this._setReqBody();
    this.$.loginAjax.generateRequest();
  }
  handleUserResponse(event){
    var response = JSON.parse(event.detail.response);
    console.log(response);
    if (response) {
      this.error = '';
      this.storedUser = {
        user: response.user[0],
        id_token: "f4932d27-4508-4909-a4e1-3a8c77a5c2b3",
        access_token: "5bfcb2117c8c6ee430281ded",
        loggedin: true
      };
      window.location = '/';
    }
    console.log("final");
  }
  handleUserError(){
    this.$.errorlabel.text="Error";
    this._openPaperToast();
  }
  _openPaperToast(error){
    this.$.errorlabel.open();
  }
}

window.customElements.define('login-app', LoginApp);
