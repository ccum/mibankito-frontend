import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@polymer/iron-localstorage/iron-localstorage.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';
import '../shared-styles/shared-styles.js';

/**
 * @customElement
 * @polymer
 */
class TransferenceApp extends PolymerElement {
  static get template() {
    return html`
      
      <style include="shared-styles">
        
      </style>
      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      <iron-ajax
        id="transferenceAjax"
        method="post"
        content-type="application/json"
        handle-as="text"
        on-response="handleTransferenceResponse"
        on-error="handleTransferenceError">
      </iron-ajax>
      <paper-material class="card" elevation="1">
        <div class="title-section">
          <h3>Ingrese el número de cuenta y el monto a transferir</h3>
        </div>
        <div class="avatar" item-icon>
          <img class="mr-3" src="https://getbootstrap.com/docs/4.2/assets/brand/bootstrap-outline.svg" alt="" width="40" height="40">
        </div>
        <div class="flex company"></div>
          <form is="iron-form" id="form" method="" action="">
            <paper-input label="Número de cuenta:" value="{{formData.accountNumberDestination}}"></paper-input>
            <paper-input label="Monto" type="number" value="{{formData.amount}}">
              <div slot="prefix">$ </div>
            </paper-input>
            <paper-button raised class="primary" on-tap="postTransference">Transferir</paper-button>
          </form>
      </paper-material>
    `;
  }
  static get properties() {
    return {
      formData: {
        type: Object,
        value: {}
      },
      urlServiceBase:{
        type: String,
        value: "https://mv5lte7w8c.execute-api.us-west-2.amazonaws.com/dev/"
      }

    };
  }
  static get observers() {
    return [
      
    ];
  }
  _setReqBody(){
    console.log(this.formData);
    this.$.transferenceAjax.body = this.formData;
    this.formData.accountNumberSource = this.accountID = window.location.pathname.split("/")[2];
    console.log(this.formData);
  }
  postTransference(){
    console.log("holaaa");
    this.$.transferenceAjax.url = this.urlServiceBase +'movement';
    this._setReqBody();
    this.$.transferenceAjax.generateRequest();
  }
  handleTransferenceResponse(event){
    var response = JSON.parse(event.detail.response);
    console.log(response);
    window.location = '/';
  }
  handleTransferenceError(){

  }
}

window.customElements.define('transference-app', TransferenceApp);
