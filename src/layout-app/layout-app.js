import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';

import '@polymer/paper-icon-button/paper-icon-button.js';
import './app-icons.js';


setPassiveTouchGestures(true);
setRootPath(MyAppGlobals.rootPath);
/**
 * @customElement
 * @polymer
 */
class LayoutApp extends PolymerElement {
  //https://color.adobe.com/es/create/color-wheel/?base=2&rule=Analogous&selected=3&name=Mi%20tema%20de%20Color&mode=rgb&rgbvalues=0.12549019607843137,0.3176470588235294,0.5058823529411764,0.48722952202416425,0.04550000000000004,0.91,1,0,0,0.91,0.523710602806526,0.04550000000000004,1,0.9476083582749653,0.050000000000000044&swatchOrder=0,1,2,3,4
  //https://material.io/tools/color/#!/?view.left=0&view.right=1&primary.color=205181&secondary.color=258218
  //https://www.flaticon.es
  static get template() {
    return html`
    <style include="shared-styles">
        :host {
          --app-primary-color: #205181;
          --app-secondary-color: #003b6b;
          --btn-primary-color: #b05800;
          --btn-secondary-color: #b05800;
        
          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }
        iron-pages{
          padding: 24px;
        }
        app-toolbar a:visited, a, a:active {
          color: #fff;
          text-decoration: none;
        }
        .user-title{
          font-size: 0.8em;
          text-align: right;
        }
        .main-title{
          font-size: 1.5em;
          font-weight: bold;
        }
        app-toolbar {
          background-color: var(--app-primary-color);
        }
        app-toolbar h4{
          color: #fff;
          margin-left: 10px;
        }
    </style>

    <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
    
    
    <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
    </app-location>
    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
    </app-route>

    <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>
            <img class="mr-3" src="https://getbootstrap.com/docs/4.2/assets/brand/bootstrap-outline.svg" alt="" width="48" height="48">
            <h4>Mi Banckito</h4>
          </app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <div name="home" >
              <a  href="[[rootPath]]">Home</a>
            </div>
            <div name="account" hidden$="[[!storedUser.loggedin]]">
              <a name="account" href="[[rootPath]]account">Mis Cuentas</a>
            </div>
            <div name="bidding" hidden$="[[!storedUser.loggedin]]">
              <a name="bidding" href="[[rootPath]]bidding">Mis Promociones</a>
            </div>
          </iron-selector>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="" class="main-title"></div>
              <div class="user-title">
                <a name="register-login" href="[[rootPath]]login" hidden$="[[storedUser.loggedin]]">Log In</a>
                <div hidden$="[[!storedUser.loggedin]]">
                  <span class="greeting">Bienvenido [[storedUser.user.first_name]] [[storedUser.user.last_name]]</span>
                  <br>
                  <paper-icon-button icon="icons:exit-to-app" on-tap="_logOut"></paper-icon-button>
                </div>
              </div>
            </app-toolbar>
          </app-header>
            <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
              <home-app name="home"></home-app>
              <account-app name="account"></account-app>
              <bidding-app name="bidding"></bidding-app>
              <login-app name="login"></login-app>
              <detailaccount-app name="detailaccount"></detailaccount-app>
              <transference-app name="transference"></transference-app>
              <user-app name="user"></user-app>
              <my-view404 name="view404"></my-view404>
            </iron-pages>
        </app-header-layout>
    </app-drawer-layout>
    `;
  }
  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      storedUser: Object,
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
    console.log(page);
    if (!page) {
      this.page = 'home';
    } else if (['home', 'account', 'bidding','login','detailaccount','transference','user'].indexOf(page) !== -1) {
      
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    switch (page) {
      case 'home':
        import('../home-app/home-app.js');
        break;
      case 'account':
        import('../account-app/account-app.js');
        break;
      case 'bidding':
        import('../bidding-app/bidding-app.js');
        break;
      case 'login':
        import('../login-app/login-app.js');
        break;
      case 'detailaccount':
        import('../detailaccount-app/detailaccount-app.js');
        break;
      case 'transference':
        import('../transference-app/transference-app.js');
        break;
      case 'user':
        import('../home-app/user-app.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }
  _logOut(){
    localStorage.removeItem("user-storage");
    window.location = '/';
  }
}

window.customElements.define('layout-app', LayoutApp);
