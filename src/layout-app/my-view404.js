import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@polymer/iron-localstorage/iron-localstorage.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';

/**
 * @customElement
 * @polymer
 */
class UserApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      </style>
      <paper-material class="card" elevation="1">
        <div class="title-section">
          <h3>Página no encontrada...</h3>

      </paper-material>
    `;
  }
  static get properties() {
    return {
      listAccountDetail:{
        type: Array,
      }
    };
  }
}

window.customElements.define('user-app', UserApp);
