import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@polymer/iron-localstorage/iron-localstorage.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';
import '../shared-styles/shared-styles.js';

/**
 * @customElement
 * @polymer
 */
class AccountApp extends PolymerElement {
  static get template() {
    return html`
      
      <style include="shared-styles">
        .cards-account a:visited, a, a:active {
          color: black;
        }
        paper-card .flex-horizontal div{
          margin: 0px 10px 0px 0px;
        }
        paper-card .flex-horizontal .flexchild h6,h5{
          margin: 20px 0px 0px 0px;
        }
        a paper-button,
        a:active paper-button,
        a:visited paper-button {
          color: #000;
        }
        .card-actions paper-button {
          font-size: 0.8em;
        }
        .cards-account paper-card{
          margin:20px;
        }
        .card-actions{
          padding: 10px 16px;
        }
      </style>
      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>


      <iron-ajax
        id="accountAjax"
        method="get"
        content-type="application/json"
        handle-as="text"
        on-response="handleAccountResponse"
        on-error="handleAccounError">
      </iron-ajax>
      <paper-material class="card" elevation="1">
          <dom-repeat items = {{listAccount}} as="account">
            <template>
              <div class='cards-account'>
                <paper-card>
                  <div class="card-content">
                    <div class="container flex-horizontal">
                      <div class="flexchild">
                        <h6>{{account.cuenta_tipo}}</h6>
                        <h5>Saldo: {{account.saldo}}</h5>
                        <h6>{{account.cuenta_numero}}</h6>
                      </div>
                      <div>
                        <img src="https://creditkarmacdn-a.akamaihd.net/res/content/reviews/chaseSapphirePreferred/title.png" alt="cards" height="55" width="80">
                      </div>
                    </div>
                  </div>
                  <div class="card-actions">
                    <a href="/detailaccount/{{account.cuenta_numero}}" tabindex="-1">
                      <paper-button raised>Ver movimientos</paper-button>
                    </a>
                    <a href="/transference/{{account.cuenta_numero}}" tabindex="-1">
                      <paper-button raised>Transferir</paper-button>
                    </a>
                  </div>
                </paper-card>
              </div>
            </template>
          </dom-repeat>
      </paper-material>
    `;
  }
  static get properties() {
    return {
      listAccount:{
        type: Array,
      },
      urlServiceBase:{
        type: String,
        value: "https://mv5lte7w8c.execute-api.us-west-2.amazonaws.com/dev/"
      }

    };
  }
  static get observers() {
    return [
      'getAccount()'
    ];
  }
  getAccount(){
    console.log("peru");
    let userID = JSON.parse(localStorage.getItem("user-storage")).user.user_id
    this.$.accountAjax.url = this.urlServiceBase + "account/"+userID;
    this.$.accountAjax.generateRequest();
  }
  handleAccountResponse(event){
    var response = JSON.parse(event.detail.response);
    this.set("listAccount",response);
    console.log(this.get('listAccount'));
    
  }
  
}

window.customElements.define('account-app', AccountApp);
