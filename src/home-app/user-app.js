import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@polymer/iron-localstorage/iron-localstorage.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';

/**
 * @customElement
 * @polymer
 */
class UserApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
      </style>
      <iron-ajax
        id="userAjax"
        method="post"
        content-type="application/json"
        handle-as="text"
        on-response="handleUserResponse"
        on-error="handleUserError">
      </iron-ajax>
      <paper-material class="card" elevation="1">
        <div class="title-section">
          <h3>Formulario de registro</h3>
          <form is="iron-form" id="form" method="" action="">
            <paper-input label="Nombre:" value="{{formData.first_name}}"></paper-input>
            <paper-input label="Apellidos:" value="{{formData.last_name}}"></paper-input>
            <paper-input label="Email:" value="{{formData.email}}"></paper-input>
            <paper-input label="Número de Documento:" value="{{formData.document_number}}"></paper-input>
            
            <paper-button raised class="primary" on-tap="postUser">Registrar</paper-button>
            <a href="/login" tabindex="-1">
              <paper-button class="link">Volver</paper-button>
            </a>
        </form>
      </paper-material>
    `;
  }
  static get properties() {
    return {
      formData: {
        type: Object,
        value: {}
      },
      urlServiceBase:{
        type: String,
        value: "https://mv5lte7w8c.execute-api.us-west-2.amazonaws.com/dev/"
      }
    };
  }
  _setReqBody(){
    console.log(this.formData);
    this.$.userAjax.body = this.formData;
  }
  postUser(){
    console.log("holaaa");
    this.$.userAjax.url = this.urlServiceBase + 'user';
    this._setReqBody();
    this.$.userAjax.generateRequest();
  }
  handleUserResponse(event){
    var response = JSON.parse(event.detail.response);
    console.log(response);
    if (response) {
      console.log("correcto");
      window.location = '/';
    }
  }
  handleUserError(){
    console.log("handleUserError");
  }
}

window.customElements.define('user-app', UserApp);
