import {PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/paper-toast/paper-toast.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@vaadin/vaadin-charts/vaadin-chart.js';
//import '@em-polymer/google-map/google-map.js';
//import '@em-polymer/google-map/google-map-marker.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
/**
 * @customElement
 * @polymer
 */
class HomeApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">

      </style>
      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      
      <iron-ajax
        id="homeAccountAjax"
        method="get"
        content-type="application/json"
        handle-as="text"
        on-response="handleHomeAccountResponse"
        on-error="handleHomeAccountError">
      </iron-ajax>
      <paper-material class="card" elevation="1">
        <div hidden$="[[storedUser.loggedin]]" class="title-section">
          <h3>Por favor inicie sessión</h3>
        </div>
        <div hidden$="[[!storedUser.loggedin]]" class="title-section">
          <h3 class="greeting">Bienvenido [[storedUser.user.first_name]] [[storedUser.user.last_name]]</h3>
          <dom-repeat
            items = {{listAccountDetail}}
            as="item">
            <template>
              <div class="task">
                <vaadin-chart type="pie" title="{{item.accout_name}}"
                tooltip additional-options='{
                  "plotOptions": {
                    "pie": {
                      "allowPointSelect":true,
                      "cursor": "pointer",
                      "dataLabels": {
                        "enabled":true,
                        "format": "<b>{point.name}</b>: {point.percentage:.1f} %"
                      }
                    }
                  }
                }'
                >
                <vaadin-chart-series
                values='{{item.data}}'></vaadin-chart-series>
                </vaadin-chart>
              </div>
            </template>
          </dom-repeat>
        </div>

      </paper-material>
    `;
  }
  static get properties() {
    return {
      listAccountDetail:{
        type: Array,
      },
      urlServiceBase:{
        type: String,
        value: "https://mv5lte7w8c.execute-api.us-west-2.amazonaws.com/dev/"
      }
    };
  }
  static get observers() {
    return [
      'getHomeAccount()'
    ];
  }
  getHomeAccount(){
    console.log("HomeAccount");
    let user_id = JSON.parse(localStorage.getItem("user-storage")).user.user_id;
    console.log(user_id);
    this.$.homeAccountAjax.url = this.urlServiceBase + "balancebyuser/"+user_id;
    this.$.homeAccountAjax.generateRequest();
  }
  handleHomeAccountResponse(event){
    var response = JSON.parse(event.detail.response);
    console.log(response);
    var list_temp = new Array();
    response.data.forEach(element => {
      var obj = {
        "accout_name": element.cuenta_tipo,
        "data": [
          {
            "name" : "Ingresos",
            "y": element.ingresos,
            "sliced": true,
            "selected": true
          },
          {
            "name" : "Egresos",
            "y": element.egresos
          },
        ]
      }
      list_temp.push(obj);
    });
    this.set("listAccountDetail", list_temp);
    console.log(this.get('listAccountDetail'));
  }
  handleHomeAccountError(){
    console.log("error");
  }

 
}

window.customElements.define('home-app', HomeApp);
