import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-flex-layout/iron-flex-layout-classes.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '../shared-styles/shared-styles.js';
/**
 * @customElement
 * @polymer
 */
class AccountApp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        .flex-horizontal {
          @apply --layout-horizontal;
        }
        .flexchild {
          @apply --layout-flex;
        }
        .cards-account a:visited, a, a:active {
          color: black;
        }
        paper-card .flex-horizontal div{
          margin: 0px 10px 0px 0px;
        }
        paper-card .flex-horizontal .flexchild h6,h5{
          margin: 20px 0px 0px 0px;
        } 
      </style>
      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      <app-data key="userData" data="{{storedUser}}"></app-data>
      
      <iron-ajax
        id="movementAjax"
        method="get"
        content-type="application/json"
        handle-as="text"
        on-response="handleMovementResponse"
        on-error="handleMovementError">
      </iron-ajax>
      <paper-material class="card" elevation="1">
        <div class="title-section">
          <h3>El siguiente listado es el detalle de la cuenta [[accountID]]</h3>
        <div>
        <vaadin-grid theme="row-dividers" items="{{listMovement}}">
          <vaadin-grid-column  header="Concepto">
            <template>
              <div hidden$="[[_validate(item)]]" class="outcome" >
                <paper-icon-button icon="icons:arrow-downward" on-tap=""></paper-icon-button>
                [[item.description]]
              </div>
              <div hidden$="[[!_validate(item)]]" class="income">
                <paper-icon-button icon="icons:arrow-upward" on-tap=""></paper-icon-button>
                [[item.description]]
              </div>
              
            </template>
          </vaadin-grid-column>
          <vaadin-grid-column  header="Fecha">
            <template>[[item.date]]</template>
          </vaadin-grid-column>
          <vaadin-grid-column  header="Monto">
            <template>[[item.type]] [[item.amount]]</template>
          </vaadin-grid-column>
          <vaadin-grid-column  header="Acciones">
            <template>
              <paper-icon-button icon="icons:chevron-right" on-tap="_viewDetails" data-item$="[[item._id.$oid]]"></paper-icon-button>
            </template>
          </vaadin-grid-column>
        </vaadin-grid>
      </paper-material>
    `;
  }
  static get properties() {
    return {
      accountID:{
        type: String,
      },
      name_account:{
        type: String,
      },
      listMovement:{
        type: Array,
      },
      urlServiceBase:{
        type: String,
        value: "https://mv5lte7w8c.execute-api.us-west-2.amazonaws.com/dev/"
      }
    };
  }
  static get observers() {
    return [
      'getMovement()'
    ];
  }
  getMovement(){
    this.accountID = window.location.pathname.split("/")[2];
    console.log("CUENTA::" + this.accountID);
    this.$.movementAjax.url = this.urlServiceBase +'movement/'+this.accountID;
    this.$.movementAjax.generateRequest();
  }
  handleMovementResponse(event){
    var response = JSON.parse(event.detail.response);
    this.set("listMovement",response);
    console.log(this.get('listMovement'));
  }
  _validate(item){
    if(item.type==="-"){
      return false;
    }
    return true;
  }
  _viewDetails(){
    console.log("hola")
  }
}

window.customElements.define('detailaccount-app', AccountApp);
