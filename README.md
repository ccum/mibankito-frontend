# \<mi-bankito\>

## Tecnologias Utlizada:

1. Polymer 3
2. app-layout
3. app-route
4. iron-ajax
5. iron-flex-layou
6. iron-form
7. paper-card
8. vaadin-charts
9. vaadin-grid

## Ejecución
Clonar repositorio y ejecutar
```
$ npm install
```
```
$ polymer serve
```
## Pantalla de Inicio de session

![alt text](https://image.prntscr.com/image/yMMpJDYDSp_-Jx0Ebph3QQ.png)

Data de prueba:
```
Número de Documento: 44125091
Contraseña: 123456 
```

## Pantalla Home:

![alt text](https://image.prntscr.com/image/Vn68dx5aSAuLQMbWJUFyhw.png)

## Pantalla Mis Cuentas:
![alt text](https://image.prntscr.com/image/iQckIFaMQyWlkQxhuKwXCA.png)

## Pantalla Movimientos

![alt text](https://image.prntscr.com/image/d9_4kWyhTFCFio7ht0MEvg.png)

## Pantalla Transferencia

![alt text](https://image.prntscr.com/image/B6eOW0FpTjacTkFCAPqRhQ.png)

## Pantalla promociones

![alt text](https://image.prntscr.com/image/kWATCXTrRYGYI1t2o_0shA.png)